package main

import (
	"errors"
	"fmt"
	"log"
)

func cmdImportPackages(args []string, move bool) error {
	return withRepo(func(repo *repo) error {
		for _, arg := range args {
			if err := repo.importPkg(arg, move); err != nil {
				if errors.Is(err, errAlreadyInIndex) {
					log.Printf("Warning: importing %s: %v", arg, err)
				} else {
					return fmt.Errorf("importing %s: %w", arg, err)
				}
			}
		}

		return nil
	}, false)
}
