package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func findDebs(path string) ([]string, error) {
	dents, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}

	// Look for existing *.deb files in the repository dir.
	var pkgs []string
	for _, dent := range dents {
		if dent.Type().IsRegular() && strings.HasSuffix(dent.Name(), ".deb") {
			pkgs = append(pkgs, filepath.Join(path, dent.Name()))
		}
	}
	return pkgs, nil
}

func cmdRescan() error {
	return withRepo(func(repo *repo) error {
		debs, err := findDebs(repo.root.Abs())
		if err != nil {
			return err
		}

		for _, arg := range debs {
			if err := repo.importPkg(arg, false); err != nil {
				if errors.Is(err, errAlreadyInIndex) {
					log.Printf("Warning: importing %s: %v", arg, err)
				} else {
					return fmt.Errorf("importing %s: %w", arg, err)
				}
			}
		}

		return nil
	}, true)
}
