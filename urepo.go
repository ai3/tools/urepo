package main

import (
	"bufio"
	"crypto/rand"
	_ "embed"
	"encoding/hex"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"sort"
	"strings"
	"time"

	"github.com/gofrs/flock"
	"github.com/ProtonMail/go-crypto/openpgp"
	"gopkg.in/yaml.v3"
	"pault.ag/go/debian/control"
	"pault.ag/go/debian/dependency"
	"pault.ag/go/debian/version"
)

// Our package type. Nothing more than the metadata present in the
// Packages index, which happens to be represented by the
// control.BinaryIndex type.
type pkg struct {
	*control.BinaryIndex
}

func (pkg *pkg) key() string {
	return fmt.Sprintf("%s|%s|%s", pkg.Package, pkg.Version, pkg.Architecture.String())
}

func newPkg(bi *control.BinaryIndex) *pkg {
	return &pkg{
		BinaryIndex: bi,
	}
}

// Repository configuration (user-visible metadata).
type repoConfig struct {
	Keep        int    `yaml:"keep"`
	Origin      string `yaml:"origin"`
	Suite       string `yaml:"suite"`
	Codename    string `yaml:"codename"`
	Label       string `yaml:"label"`
	Description string `yaml:"description"`
	PrivateKey  string `yaml:"private-key"`
}

func newDefaultConfig() *repoConfig {
	return &repoConfig{
		Keep:        3,
		Origin:      "urepo",
		Suite:       "unstable",
		PrivateKey:  "~/.urepo/key.asc",
		Label:       "Debian package archive",
		Description: "An archive of Debian packages.",
	}
}

func loadConfig(path string, cfg *repoConfig) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()
	return yaml.NewDecoder(f).Decode(cfg)
}

func (c *repoConfig) setFlags(f *flag.FlagSet) {
	f.IntVar(&c.Keep, "keep", c.Keep, "old package `versions` to keep")
	f.StringVar(&c.Origin, "origin", c.Origin, "repository `origin`")
	f.StringVar(&c.Suite, "suite", c.Suite, "repository `suite`")
	f.StringVar(&c.Codename, "codename", c.Codename, "repository `codename` (defaults to the value of --repository)")
	f.StringVar(&c.PrivateKey, "private-key", c.PrivateKey, "`path` to the armored GPG secret key for signing")
}

// Repository manager.
type repo struct {
	config *repoConfig
	signer *openpgp.Entity
	lock   *flock.Flock

	root Path

	index            []*pkg
	byNameAndVersion map[string]*pkg
}

// Initialize the repository manager.
func newRepo(repoRoot, repoRel string, ignoreState bool) (*repo, error) {
	root := Path{root: repoRoot, rel: repoRel}
	if err := os.MkdirAll(root.Abs(), 0755); err != nil {
		return nil, err
	}

	config := defaultConfig
	if config.Codename == "" {
		config.Codename = repoRel
	}

	configFiles := []string{
		expandTilde("~/.urepo/config"),
		root.Join(".urepo.conf").Abs(),
	}
	for _, path := range configFiles {
		if err := loadConfig(path, config); err != nil && !errors.Is(err, os.ErrNotExist) {
			return nil, err
		}
	}

	signer, err := newSigner(expandTilde(config.PrivateKey))
	if err != nil {
		return nil, err
	}

	lock := flock.New(filepath.Join(repoRoot, ".lock"))
	if err := lock.Lock(); err != nil {
		return nil, fmt.Errorf("unable to obtain lock: %w", err)
	}

	r := &repo{
		lock:             lock,
		root:             root,
		config:           config,
		signer:           signer,
		byNameAndVersion: make(map[string]*pkg),
	}

	if !ignoreState {
		if err := r.loadIndex(); err != nil {
			return nil, err
		}
	}

	return r, nil
}

func (r *repo) Close() {
	r.lock.Close()
}

// Read a Packages file.
func readPackages(r io.Reader) ([]*pkg, error) {
	index, err := control.ParseBinaryIndex(bufio.NewReader(r))
	if err != nil {
		return nil, err
	}

	out := make([]*pkg, len(index))
	for i := 0; i < len(index); i++ {
		out[i] = newPkg(&index[i])
	}
	return out, nil
}

func (r *repo) loadIndex() error {
	f, err := os.Open(r.root.Join("Packages").Abs())
	if err != nil {
		if os.IsNotExist(err) {
			return nil
		}
		return err
	}
	defer f.Close()

	index, err := readPackages(f)
	if err != nil {
		return err
	}
	r.index = index

	for _, pkg := range index {
		r.byNameAndVersion[pkg.key()] = pkg
	}

	return nil
}

func (r *repo) hasPackage(pkg *pkg) bool {
	_, ok := r.byNameAndVersion[pkg.key()]
	return ok
}

// Path is a type that allows us to manage paths relative to a root.
type Path struct {
	root string
	rel  string
}

func (p Path) Abs() string {
	return filepath.Join(p.root, p.rel)
}

func (p Path) Relative() string {
	return p.rel
}

func (p Path) Join(s string) Path {
	return Path{root: p.root, rel: filepath.Join(p.rel, s)}
}

func (p Path) WithSameRoot(rel string) Path {
	return Path{root: p.root, rel: rel}
}

// Release defines the control structure in the repository 'Release' file.
type Release struct {
	control.Paragraph

	Origin        string
	Label         string
	Suite         string
	Codename      string
	Date          string
	AcquireByHash string            `control:"Acquire-By-Hash"`
	Architectures []dependency.Arch `control:"Architecture"`
	Components    []string          `control:"Components"`
	Description   string

	MD5Sums    []control.MD5FileHash    `control:"MD5Sum" delim:"\n" strip:"\n\r\t " multiline:"true"`
	SHA1Sums   []control.SHA1FileHash   `control:"SHA1" delim:"\n" strip:"\n\r\t " multiline:"true"`
	SHA256Sums []control.SHA256FileHash `control:"SHA256" delim:"\n" strip:"\n\r\t " multiline:"true"`
}

func (r *Release) addFileHashes(hashes []control.FileHash) {
	for _, fh := range hashes {
		switch fh.Algorithm {
		case "MD5Sum":
			r.MD5Sums = append(r.MD5Sums, control.MD5FileHash{FileHash: fh})
		case "SHA1":
			r.SHA1Sums = append(r.SHA1Sums, control.SHA1FileHash{FileHash: fh})
		case "SHA256":
			r.SHA256Sums = append(r.SHA256Sums, control.SHA256FileHash{FileHash: fh})
		}
	}
}

// List of packages that can be sorted by descending version.
type pkgList []*pkg

func (l pkgList) Len() int      { return len(l) }
func (l pkgList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l pkgList) Less(i, j int) bool {
	return version.Compare(l[i].Version, l[j].Version) > 0
}

// Only keep 'keepLast' newest versions of every package. Returns the
// list of packages to keep and to drop from the index.
func trimOldVersions(pkgs []*pkg, keepLast int) ([]*pkg, []*pkg) {
	tmp := make(map[string][]*pkg)
	for _, pkg := range pkgs {
		tmp[pkg.Package] = append(tmp[pkg.Package], pkg)
	}

	var keep, remove []*pkg
	for _, ppkgs := range tmp {
		if len(ppkgs) > keepLast {
			sort.Sort(pkgList(ppkgs))
			remove = append(remove, ppkgs[keepLast:]...)
			ppkgs = ppkgs[:keepLast]
		}
		keep = append(keep, ppkgs...)
	}

	return keep, remove
}

func (r *repo) trimOldVersions() error {
	keep, remove := trimOldVersions(r.index, r.config.Keep)
	for _, rpkg := range remove {
		path := r.root.WithSameRoot(rpkg.Filename)
		if err := os.Remove(path.Abs()); err != nil {
			log.Printf("error: %v", err)
		}
		delete(r.byNameAndVersion, rpkg.key())
		log.Printf("deleted package %s@%s (%s) from repository %s",
			rpkg.Package, rpkg.Version, rpkg.Filename, r.root.rel)
	}
	r.index = keep
	return nil
}

var errAlreadyInIndex = errors.New("index already contains this package")

func (r *repo) importPkg(pkgSrcPath string, move bool) error {
	// Load the package.
	pkg, err := loadDeb(pkgSrcPath)
	if err != nil {
		return err
	}

	if r.hasPackage(pkg) {
		if move {
			os.Remove(pkgSrcPath)
		}
		return errAlreadyInIndex
	}

	// Figure out the new location for the package, inside the
	// repository root.
	path := r.root.Join(filepath.Base(pkgSrcPath))
	pkg.Filename = path.Relative()

	if move {
		if err := moveToRepository(pkgSrcPath, path.Abs()); err != nil {
			return fmt.Errorf("moving file into repository: %w", err)
		}
	}

	log.Printf("imported package %s@%s (%d bytes) into the %s repository",
		pkg.Package, pkg.Version, pkg.Size, r.root.rel)

	r.index = append(r.index, pkg)
	r.byNameAndVersion[pkg.key()] = pkg
	return nil
}

func (r *repo) writePackages() ([]*fileMeta, error) {
	sfx := "." + randomID()
	w, files, err := newPackagesWriter(r.root.Join("Packages").Abs(), sfx)
	if err != nil {
		return nil, err
	}
	defer w.Close()

	enc, err := control.NewEncoder(w)
	if err != nil {
		return nil, err
	}
	for _, pkg := range r.index {
		fudgeOrder(&pkg.BinaryIndex.Paragraph)
		if err := enc.Encode(&pkg.BinaryIndex); err != nil {
			return nil, fmt.Errorf("encoding %s: %w", pkg.Package, err)
		}
	}

	return files, w.Close()
}

func (r *repo) writeRelease(rel *Release) error {
	w, err := newReleaseWriter(r.root.Abs(), r.signer)
	if err != nil {
		return err
	}

	if err := control.Marshal(w, rel); err != nil {
		w.Close()
		return err
	}

	return w.Commit()
}

func (r *repo) write() error {
	// The ordering of file and link generation will ensure that apt
	// clients that use InRelease and by-hash will always be able to
	// successfully retrieve a consistent view of the packages. For older
	// clients, the results are best-effort only.
	files, err := r.writePackages()
	if err != nil {
		return err
	}

	if err := r.buildByHashLinks(files); err != nil {
		return err
	}

	// Write the release metadata last.
	rel := &Release{
		Origin:        r.config.Origin,
		Label:         r.config.Label,
		Suite:         r.config.Suite,
		Codename:      r.config.Codename,
		AcquireByHash: "yes",
		Date:          time.Now().Format(time.RFC1123Z),
		Architectures: r.getArchitectures(),
		Components:    []string{"main"},
		Description:   r.config.Description,
	}

	for _, f := range files {
		rel.addFileHashes(f.Hashes(r.root))

		if err := symlinkAtomically(filepath.Base(f.path), f.metaPath); err != nil {
			return err
		}
	}

	if err := r.writeRelease(rel); err != nil {
		return err
	}

	return nil
}

func (r *repo) buildByHashLinks(files []*fileMeta) error {
	for _, f := range files {
		for i := 0; i < len(stdHashes); i++ {
			// The 'by-hash' hierarchy is rooted at the file's
			// (as exposed in metadata) containing directory.
			relDir, _ := filepath.Rel(r.root.Abs(), filepath.Dir(f.metaPath))
			dir := r.root.Join(relDir).Join("by-hash").Join(stdHashNames[i])
			path := dir.Join(hex.EncodeToString(f.hashes[i].Sum(nil)))

			if err := os.MkdirAll(dir.Abs(), 0755); err != nil {
				return err
			}

			rel, _ := filepath.Rel(dir.Abs(), f.path)
			if err := os.Symlink(rel, path.Abs()); err != nil {
				return err
			}
		}
	}
	return nil
}

func (r *repo) getArchitectures() []dependency.Arch {
	tmp := make(map[string]dependency.Arch)
	for _, pkg := range r.index {
		tmp[pkg.Architecture.String()] = pkg.Architecture
	}
	out := make([]dependency.Arch, 0, len(tmp))
	for _, d := range tmp {
		out = append(out, d)
	}
	return out
}

func randomID() string {
	var b [8]byte
	if _, err := io.ReadFull(rand.Reader, b[:]); err != nil {
		panic(err)
	}
	return hex.EncodeToString(b[:])
}

// Expand ~/ paths.
func expandTilde(s string) string {
	if strings.HasPrefix(s, "~/") {
		return filepath.Join(os.Getenv("HOME"), s[2:])
	}
	return s
}

// We really really want the Description to appear last, so ensure all
// the additional fields we've set appear before it (and add them in
// case they're missing).
func fudgeOrder(para *control.Paragraph) {
	var order []string
	for _, s := range para.Order {
		switch s {
		case "Description":
		case "Filename":
		case "Size":
		case "MD5sum":
		case "SHA1":
		case "SHA256":
		default:
			order = append(order, s)
		}
	}
	order = append(order, "Filename")
	order = append(order, "Size")
	order = append(order, "MD5sum")
	order = append(order, "SHA1")
	order = append(order, "SHA256")
	order = append(order, "Description")
	para.Order = order
}

// Safely move a file into the repository. The source may be on a
// different device, so a plain os.Rename might not work. The "mv"
// tool is an easy way to get the job done.
func moveToRepository(src, dst string) error {
	return exec.Command("mv", "-f", src, dst).Run()
}

// Atomically create or replace a symbolic link. Shell out to "ln -sf"
// because it is easier than reproducing its force-rename-temporary-file
// trick for atomic symlink updates.
func symlinkAtomically(src, dst string) error {
	return exec.Command("ln", "-sf", src, dst).Run()
}

//go:embed README.md
var readme string

func usage() {
	italicRx := regexp.MustCompile(`\*([^\*\s]+)\*`)
	s := italicRx.ReplaceAllString(readme, "`$1'")
	linkRx := regexp.MustCompile(`(?s)\[([^\]]+)\]\(http[^\)]+\)`)
	s = linkRx.ReplaceAllString(s, "$1")

	fmt.Fprintf(flag.CommandLine.Output(), `Usage: urepo [<options>] [<deb>...]

%s
# Known options

`, s)
	flag.PrintDefaults()
}

var (
	// Flags that can only be set on local command line.
	repoRoot     = flag.String("root", ".", "repository root dir")
	doReceive    = flag.Bool("receive", false, "receive a bundle of packages on stdin")
	doRescan     = flag.Bool("rescan", false, "rescan the repository dir in case of corruption")
	moveOnImport = flag.Bool("move", false, "move files when importing them (removes source)")

	// Flags that can be also accepted over SSH connections with --receive.
	repoDir       = "."
	defaultConfig = newDefaultConfig()
)

func setFlags(f *flag.FlagSet) {
	defaultConfig.setFlags(f)
	f.StringVar(&repoDir, "repository", repoDir, "repository path")
}

// Run a function in the context of a repository. This function
// controls the repository lifecycle for every command that is ran. We
// are assuming that the function 'f' mutates the repository, so its
// metadata is rewritten at the end (and cleanup tasks are run).
func withRepo(f func(*repo) error, ignoreState bool) error {
	if strings.HasPrefix(repoDir, "/") {
		return errors.New("--repository should not be an absolute path")
	}

	repo, err := newRepo(*repoRoot, repoDir, ignoreState)
	if err != nil {
		return fmt.Errorf("reading repository state: %w", err)
	}
	defer repo.Close()

	if err := f(repo); err != nil {
		return err
	}

	if err := repo.trimOldVersions(); err != nil {
		return err
	}

	if err := repo.write(); err != nil {
		return fmt.Errorf("updating repository metadata: %w", err)
	}

	repo.cleanup()

	return nil
}

func main() {
	log.SetFlags(0)
	flag.Usage = usage
	setFlags(flag.CommandLine)
	flag.Parse()

	var err error
	switch {
	case *doRescan && *doReceive:
		err = errors.New("--rescan and --receive are exclusive")
	case (*doRescan || *doReceive) && flag.NArg() > 0:
		err = errors.New("can't import packages with --rescan / --receive")
	case *doRescan:
		err = cmdRescan()
	case *doReceive:
		err = cmdReceive(os.Stdin)
	case flag.NArg() == 0:
		err = errors.New("too few arguments")
	default:
		err = cmdImportPackages(flag.Args(), *moveOnImport)
	}
	if err != nil {
		log.Fatalf("Error: %v", err)
	}
}
