package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"testing"

	"pault.ag/go/debian/control"
)

const (
	testDebianPackage = "testdata/urepo_0.2+0~20160730125233~1.73fe3c_all.deb"
	testSecretKey     = "testdata/test-secret-key.asc"
)

func parseRelease(r io.Reader) (*Release, error) {
	dec, err := control.NewDecoder(r, nil)
	if err != nil {
		return nil, err
	}
	var rel Release
	if err := dec.Decode(&rel); err != nil {
		return nil, err
	}
	return &rel, nil
}

func checkRelease(t *testing.T, path string) {
	t.Helper()

	reldata, err := os.ReadFile(path)
	if err != nil {
		t.Fatalf("reading Release file: %v", err)
	}
	rel, err := parseRelease(bytes.NewReader(reldata))
	if err != nil {
		t.Fatalf("can't parse generated Release file: %v", err)
	}
	if rel.Origin != "urepo" {
		t.Errorf("bad Origin in generated Release file: %v", rel.Origin)
	}
	if rel.Codename != "Test" {
		t.Errorf("bad Codename in generated Release file (did not read repo config?): %v", rel.Codename)
	}
}

func TestUrepo_Import(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	os.WriteFile(filepath.Join(dir, ".urepo.conf"), []byte(fmt.Sprintf(`
codename: "Test"
private-key: "%s"
`, testSecretKey)), 0600)

	os.Mkdir(filepath.Join(dir, "input"), 0700) //nolint: errcheck

	// Set flags.
	*repoRoot = dir
	repoDir = "."

	// Try to import twice the same package, the second time the
	// package should be ignored (but cmdImportPackages should not
	// return an error).
	for i := 0; i < 2; i++ {
		// Make a copy of the test debian package (or the import
		// command will remove it).
		pkgPath := filepath.Join(dir, "input", filepath.Base(testDebianPackage))
		if err := exec.Command("cp", testDebianPackage, pkgPath).Run(); err != nil {
			t.Fatalf("copy: %v", err)
		}

		err = cmdImportPackages([]string{pkgPath}, true)
		if err != nil {
			t.Fatalf("import packages: %v", err)
		}

		checkRelease(t, filepath.Join(dir, "Release"))

		// Source must be gone.
		if _, err := os.Stat(pkgPath); err == nil {
			t.Errorf("the source package has not been removed")
		}

		if i == 0 {
			data, _ := os.ReadFile(dir + "/Release")
			t.Logf("%s", data)

			data, _ = exec.Command("/bin/ls", "-lR", dir).Output()
			t.Logf("%s", data)

			// Perform checks on the compressed data.
			//if err := exec.Command("gzip", "-dc", filepath.Join(dir, "Packages.gz")).Run(); err != nil {
			//	t.Fatal("Packages.gz looks invalid")
			//}
			if err := exec.Command("xz", "-dc", filepath.Join(dir, "Packages.xz")).Run(); err != nil {
				t.Fatal("Packages.xz looks invalid")
			}
		}
	}
}

func TestUrepo_Receive(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	os.WriteFile(filepath.Join(dir, ".urepo.conf"), []byte(fmt.Sprintf(`
codename: "Test"
private-key: "%s"
`, testSecretKey)), 0600)

	os.Mkdir(filepath.Join(dir, "input"), 0700) //nolint: errcheck

	// Set flags.
	*repoRoot = dir
	repoDir = "."

	cmd := exec.Command("tar", "cvf", "-", testDebianPackage)
	stdout, _ := cmd.StdoutPipe()
	cmd.Start()

	err = cmdReceive(stdout)
	if err != nil {
		t.Fatalf("receive bundle: %v", err)
	}

	checkRelease(t, filepath.Join(dir, "Release"))
}
