# μrepo

*μrepo* (micro-repo) is a tool to maintain one or more minimal Debian
package repositories, for easy publication over HTTP. Useful, for
example, to support small projects that need a package repository.
Its primary features are:

* Self-contained binary, no dependencies.
* No long-running daemon: the tool only runs at ingestion, generating
  a filesystem hierarchy that can be served by any HTTP server.
* System setup requirements are minimal, just a user and its
  *authorized_keys*.
* Can keep the last N versions of each package (to allow rollbacks).
* Ingestion happens over SSH.
* Generates a [modern Debian repository](https://wiki.debian.org/DebianRepository/Format), with
  InRelease files and *by-hash* links. Modern APT clients and caches
  won't break during updates.

## Overview

A repository here is just a directory containing Debian packages.
Passing a path to such a directory to *urepo* will generate new
(gpg-signed) Packages and Release files for clients to download. All
that is necessary to export the package repository is for that
directory to be remotely accessible over HTTP.

The *--root* and *--repository* options combine to control how the
repository is available to clients, in combination with the HTTP
server: --root should point at the root of the repository exported
over HTTP (the *uri* component of the deb repository specification),
while --repository determines the *suite* component. Note that the
value of --repository must match the *codename* of the repository.

Release files will be GPG-signed by default, so a secret key (in
armored format) must be available.

Access to the repository is based on UNIX permissions, i.e. it is
determined by whoever can read the GPG secret key and write to the
repository path. This is normally used in combination with SSH (see
"Receiving a package bundle" below) which ultimately controls access.

## Installation

To install from source, clone this repository and run

    go build .

to generate the *urepo* binary. You can then install manually
somewhere in your PATH, like /usr/local/bin. Or you can build the
binary without having to clone the repository first, with:

    go install git.autistici.org/pipelines/tools/urepo@latest

Alternatively, a Debian package is provided.

The tool is fully self-contained and has no additional runtime
dependencies except for the *xz* binary, which should already be
installed on a base Debian system.

## Manually adding packages to the repository

In its default mode, urepo will accept Debian package files as
command-line arguments and it will import them into the
repository. The files will be physically moved into the repository.

## Repairing a corrupted repository

It is possible that due to external action the repository might become
corrupted, either because of missing files or checksum issues. In this
case it is possible to invoke urepo with the *--rescan* flag, which
will cause it to automatically scan all the *.deb files it finds in
the repository directory and regenerate the Packages and Release
files.

## Receiving a package bundle

For remote uploads, specifying the *--receive* option will cause urepo
to accept a tar-encoded bundle of packages on standard input. It will
unpack those files locally in a temporary directory (ignoring all path
components and non-package files for safety) and import them into the
repository.

If you happen to do this sort of thing regularly, or you'd like a
slightly higher level of automation, you can use the *urepo-put* tool
to upload your packages over SSH. Let's assume the following, which is
very common for e.g. a CI setup:

- you have a centralized repository for your projects on a server
  somewhere
- you have created a dedicated user for this purpose on that server
- you build your packages on a different host, so you'd like to use
  SSH for authentication and transport

You can then add your SSH key to the repository user's authorized_keys
file, using *urepo --receive* as the forced command:

    command="urepo --root=/var/repo --receive" ssh-rsa AA....

This will allow you to use, on your host, a command such as:

    urepo-put --repository=NAME package.deb

and have your packages automatically added to the repository at
/var/repo/NAME/.

Note that when urepo is run as a SSH forced command, some command-line
options are not available for security reasons (most importantly
*--root*).

### Bundle format

On the wire, a bundle is just a tar file of .deb packages, without
compression. Its purpose is to allow atomic updates of the package
indexes, so that clients can see a coherent view of dependencies.

## Repository configuration

Instead of always specifying release metadata options such as
*--codename*, *--origin* etc. it is possible to save them in a
configuration file named *.urepo.conf* in the repository directory.

This should be a YAML-encoded file where you can specify most of the
release metadata attributes:

* *origin*
* *suite*
* *codename*
* *label*
* *description*

In addition, two attributes control the behavior of urepo itself:

* *keep* (equivalent to the *--keep* command-line option), number of
  old versions of each package to keep
* *private-key* (equivalent to the *--private-key* command-line
  option), path to the armored GPG secret key to be used for signing
  (without passphrase)

Urepo will look for such configuration in a file named *.urepo.conf*
in the repository directory, as well as in *~/.urepo/config* (useful
to set a site-wide GPG secret key, for instance).
