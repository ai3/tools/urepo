module git.autistici.org/pipelines/tools/urepo

go 1.19

require (
	github.com/ProtonMail/go-crypto v1.1.5
	github.com/gofrs/flock v0.8.1
	gopkg.in/yaml.v3 v3.0.1
	pault.ag/go/debian v0.16.0
)

require (
	github.com/cloudflare/circl v1.3.7 // indirect
	github.com/kjk/lzma v0.0.0-20161016003348-3fd93898850d // indirect
	github.com/klauspost/compress v1.16.5 // indirect
	github.com/xi2/xz v0.0.0-20171230120015-48954b6210f8 // indirect
	golang.org/x/crypto v0.32.0 // indirect
	golang.org/x/sys v0.29.0 // indirect
	pault.ag/go/topsort v0.1.1 // indirect
)
