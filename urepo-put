#!/bin/bash
#
# Client to upload packages.
#

usage() {
    cat <<EOF
Usage: $(basename "$0") [<OPTIONS>] <REMOTE_ADDR> <PACKAGE>...

Upload a package to a remote \`urepo' repository, updating the remote
repository after a successful upload.

REMOTE_ADDR must be a valid SSH address, in the form USER@HOST.

The ssh binary used (\`ssh' by default) can be overridden setting the
SSH environment variable, if you need to pass additional options.

Known options (passed to the remote handler):

  -r, --repository=NAME    Upload packages to the repository identified
                           by this name (in case of multiple repositories).

EOF
}

repo=
while :; do
    case "$1" in
        -h|--help)
            usage
            exit 0
            ;;

        -r|--repo|--repository)
            if [ -n "$2" ]; then
                repo="$2"
                shift 2
                continue
            fi
            echo "Error: --repository requires an argument" >&2
            exit 1
            ;;
        --repo=*|--repository=*)
            repo="${1#*=}"
            if [ -z "${repo}" ]; then
                echo "Error: --repository requires an argument" >&2
                exit 1
            fi
            ;;
        --)
            shift
            break
            ;;
        -?*)
            usage
            echo "Error: unknown option '$1'" >&2
            exit 1
            ;;
        *)
            break
    esac
    shift
done

if [ $# -lt 2 ]; then
    echo "Error: wrong number of arguments" >&2
    usage
    exit 1
fi

host="$1"
shift

set -e
set -o pipefail

tar -c -f- "$@" \
    | "${SSH:-ssh}" "${host}" urepo --receive "${repo:+--repository=}${repo}"


