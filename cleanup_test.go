package main

import (
	"os"
	"path/filepath"
	"testing"
)

func TestCleanupBrokenSymlinks(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	os.WriteFile(filepath.Join(dir, "file"), []byte("yeah"), 0600)
	os.Symlink("file", filepath.Join(dir, "link"))
	os.Symlink("non-existing", filepath.Join(dir, "badlink"))

	if err := cleanupBrokenSymlinks(dir); err != nil {
		t.Fatal(err)
	}

	if _, err := os.Lstat(filepath.Join(dir, "badlink")); err == nil {
		t.Fatal("badlink not removed")
	}
}
