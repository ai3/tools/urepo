package main

import (
	"bytes"
	"io"
	"os"
	"path/filepath"
	"testing"

	"github.com/ProtonMail/go-crypto/openpgp"
	"github.com/ProtonMail/go-crypto/openpgp/clearsign"
)

func checkDetachedSignature(t *testing.T, e *openpgp.Entity, path string) {
	t.Helper()

	f, _ := os.Open(path)
	defer f.Close()
	s, _ := os.Open(path + ".gpg")
	defer s.Close()

	var keyring openpgp.EntityList
	keyring = append(keyring, e)

	_, err := openpgp.CheckArmoredDetachedSignature(keyring, f, s, nil)
	if err != nil {
		t.Fatalf("CheckArmoredDetachedSignature(%s): %v", path, err)
	}
}

func checkInlineSignature(t *testing.T, e *openpgp.Entity, path string) {
	t.Helper()

	var keyring openpgp.EntityList
	keyring = append(keyring, e)

	data, _ := os.ReadFile(path)

	block, _ := clearsign.Decode(data)
	if block == nil {
		t.Fatalf("clearsign.Decode(%s): empty result", path)
	}
	_, err := openpgp.CheckDetachedSignature(keyring,
		bytes.NewReader(block.Bytes),
		block.ArmoredSignature.Body,
		nil)
	if err != nil {
		t.Fatalf("block.VerifySignature(%s): %v", path, err)
	}
}

func TestDetachedSignature(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	signer, err := newSigner(testSecretKey)
	if err != nil {
		t.Fatalf("newSigner(): %v", err)
	}

	f, _ := os.Create(dir + "/f.txt")
	s, _ := os.Create(dir + "/f.txt.gpg")

	sw, _ := newDetachedSignatureWriter(s, signer)

	w := io.MultiWriter(f, sw)

	io.WriteString(w, "this is a test\n") //nolint:errcheck

	f.Close()
	sw.Close()

	checkDetachedSignature(t, signer, dir+"/f.txt")
}

func TestInlineSignature(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	signer, err := newSigner(testSecretKey)
	if err != nil {
		t.Fatalf("newSigner(): %v", err)
	}

	f, _ := os.Create(dir + "/f.txt")
	sw, err := newInlineSignatureWriter(f, signer)
	if err != nil {
		t.Fatalf("newInlineSignatureWriter: %v", err)
	}
	io.WriteString(sw, "this is a test\n") //nolint:errcheck
	sw.Close()

	checkInlineSignature(t, signer, dir+"/f.txt")
}

func TestReleaseWriter(t *testing.T) {
	dir, err := os.MkdirTemp("", "")
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dir)

	signer, err := newSigner(testSecretKey)
	if err != nil {
		t.Fatalf("newSigner(): %v", err)
	}

	w, err := newReleaseWriter(dir, signer)
	if err != nil {
		t.Fatalf("newReleaseWriter(): %v", err)
	}
	defer w.Close()

	io.WriteString(w, "this is a test\n") //nolint:errcheck

	if err := w.Commit(); err != nil {
		t.Fatalf("Close(): %v", err)
	}

	for _, name := range []string{"Release", "Release.gpg", "InRelease"} {
		if _, err := os.Stat(filepath.Join(dir, name)); err != nil {
			t.Fatalf("%s file not created", name)
		}
	}

	checkInlineSignature(t, signer, dir+"/InRelease")
	checkDetachedSignature(t, signer, dir+"/Release")
}
